/*1. Метод об'єкту - це функція ,яка є властивістю цього об’єкта. */
/*2. Значення можуть бути будь-якого типу.*/
/*3. Змінна зберігає не сам об’єкт, а його адресу в пам’яті, тобто посилання на нього.*/


function createNewUser() {
     let firstName = prompt("Вкажіть своє ім'я");
     let lastName = prompt("Вкажіть своє прізвище");
     let newUser = {
          nameUser: firstName,
          surnameUser: lastName,
          getLogin() {
               let login = (this.nameUser.slice(0, 1) + this.surnameUser).toLowerCase();
               return login;
          },
     }

     return newUser;
}

let user1 = createNewUser();
console.log(user1.getLogin());